package gov.sic.bd;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

import gov.sic.model.BrandPc;
import gov.sic.model.Survey;
import gov.sic.model.User;

public class ConexionMySql {
	private Connection connection;
	
	public ConexionMySql() throws Exception {
		connectionMySql();
	}

	public void connectionMySql() throws Exception {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			connection = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/" + "survey", "", "");
			System.out.println("Conectado con EXITO a la BD");
		} catch (ClassNotFoundException ex) {
			System.out.println("PROBLEMAS CON LA CONEXION");
		} catch (SQLException ex) {
			System.out.println("PROBLEMAS CON LA CONEXION");
		}
	}

	public User login(String userId, String password) {
		try {
			String query = "SELECT * FROM USER WHERE user = \"" + userId + "\" AND password = \"" + password + "\"";
            Statement statement = (Statement) connection.createStatement();
            ResultSet resultSet;
            resultSet = statement.executeQuery(query);
            if (resultSet.getRow() > 0) {
				User user = new User(resultSet.getString("userId"), resultSet.getString("name"), resultSet.getString("user"), resultSet.getString("password"));
				return user;
			} else {
				return null;
			}
		} catch (Exception e) {
			return null;
		}
	}
	
	public String insertSurvey(Survey survey) {
		try {
			String query = "INSERT INTO SURVEY VALUES("
					+ "\"" + survey.getNoDocument() + "\", "
					+ "\"" + survey.getUser() + "\", "
					+ "\"" + survey.getBrandPc() + "\", "
					+ "\"" + survey.getEmail() + "\", "
					+ "\"" + survey.getComment() + "\")";
			Statement statement = (Statement) connection.createStatement();
			statement.executeUpdate(query);
			return "LA ENCUESTA A SIDO GUARDADA CON EXITO";
		} catch (SQLException ex) {
			return "ERROR AL GUARDAR LA ENCUESTA";
		}
	}
	
	public String deleteSurvey(String noDocument) {
        try {
            String query = "DELETE FROM SURVEY WHERE noDocument = \"" + noDocument + "\"";
            Statement statement = (Statement) connection.createStatement();
            statement.executeUpdate(query);
            return "LA ENCUESTA A SIDO ELIMINADA CON EXITO"; 
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return "ERROR AL ELIMINAR LA ENCUESTA";
        }
    }
	
	public ArrayList<Survey> getAllSurveys() {
        try {
        	ArrayList<Survey> surveys = new ArrayList<Survey>();
            String query = "SELECT * FROM SURVEY";
            Statement statement = (Statement) connection.createStatement();
            ResultSet resultSet;
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
            	
            	String queryBrand = "SELECT * FROM BRANDPC WHERE brandPcId = \"" + resultSet.getString("brandPc") + "\"";
            	ResultSet resultSetBrand;
            	resultSetBrand = statement.executeQuery(queryBrand);
            	BrandPc brandPc = new BrandPc(resultSetBrand.getString("brandPcId "), resultSetBrand.getString("description"));
            	
            	String queryUser = "SELECT * FROM USER WHERE userId = \"" + resultSet.getString("user") + "\"";
            	ResultSet resultSetUser;
            	resultSetUser = statement.executeQuery(queryUser);
            	User user = new User(resultSetUser.getString("userId"), resultSetUser.getString("name"), resultSetUser.getString("user"), resultSetUser.getString("password"));
            	
            	Survey survey = new Survey(Integer.parseInt(resultSet.getString("noDocument")), user, resultSet.getString("email"), resultSet.getString("comment"), brandPc);
            	surveys.add(survey);
            }

            return surveys;
        } catch (SQLException ex) {
        	return null;
        }
    }

}
