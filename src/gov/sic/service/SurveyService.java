package gov.sic.service;

import gov.sic.model.BrandPc;
import gov.sic.model.Survey;
import gov.sic.model.User;

public interface SurveyService {

	String login(User userLogin);
	
	public String addSurvey(String email, String comment, BrandPc brandPc);
	
	public Survey[] getAllSurveys(); 
	
	public String deleteSurvey(String noDocument);

}
