package gov.sic.service;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import gov.sic.bd.ConexionMySql;
import gov.sic.model.BrandPc;
import gov.sic.model.Survey;
import gov.sic.model.User;

@Path("/survey")
@Consumes(MediaType.APPLICATION_XML)
@Produces(MediaType.APPLICATION_XML)
public class SurveyServiceImpl implements SurveyService{
	private ArrayList<Survey> surveys;
	private ConexionMySql conexionMySql;
	
	public SurveyServiceImpl() throws Exception {
		surveys = new ArrayList<Survey>();
		conexionMySql = new ConexionMySql();
	}

	@Override
	@POST
	@Path("/login")
	public String login(User userLogin) {
		User user = conexionMySql.login(userLogin.getUser(), userLogin.getPassword());
		if (user != null) {
			System.out.println("LOGEEADO CON EXISTO");
			return "LOGEEADO CON EXISTO";
		} else {
			System.out.println("ERROR AL INICIAR SESI�N");
			return "ERROR AL INICIAR SESI�N";
		}
	}

	@Override
	public String addSurvey(String email, String comment, BrandPc brandPc) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Survey[] getAllSurveys() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String deleteSurvey(String noDocument) {
		// TODO Auto-generated method stub
		return null;
	}

}
