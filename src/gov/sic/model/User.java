package gov.sic.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement (name = "user")
public class User {
	private String userId;
	private String name;
	private String user;
	private String password;
	
	public User(String userId, String name, String user, String password) {
		this.userId = userId;
		this.name = name;
		this.user = user;
		this.password = password;
	}
	
	public String getUserId() {
		return userId;
	}
	
	public String getName() {
		return name;
	}
	
	public String getUser() {
		return user;
	}
	
	public String getPassword() {
		return password;
	}
	
}
