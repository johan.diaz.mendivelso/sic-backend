package gov.sic.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement (name = "brandPc")
public class BrandPc {
	private String brandPcId;
	private String description;
	
	public BrandPc(String brandPcId, String description) {
		this.brandPcId = brandPcId;
		this.description = description;
	}
	
	public String getBrandPcId() {
		return brandPcId;
	}
	
	public String getDescription() {
		return description;
	}
	
}
