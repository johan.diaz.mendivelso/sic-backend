package gov.sic.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement (name = "survey")
public class Survey {
	private int noDocument;
	private User user;
	private BrandPc brandPc;
	private String email;
	private String comment;
	
	public Survey(int noDocument, User user, String email, String comment, BrandPc brandPc) {
		this.noDocument = noDocument;
		this.user = user;
		this.email = email;
		this.comment = comment;
		this.brandPc = brandPc;
	}
	
	public int getNoDocument() {
		return noDocument;
	}
	
	public User getUser() {
		return user;
	}
	
	public String getEmail() {
		return email;
	}
	
	public String getComment() {
		return comment;
	}
	
	public BrandPc getBrandPc() {
		return brandPc;
	}
}
